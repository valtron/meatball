import sys
from os.path import join, isfile

from .compiler import compile_file
from .config import EXT

class _MeatballFinder:
	def find_module(self, fullname, path):
		if path is None:
			return None
		
		name = fullname.rsplit('.', 1)[1]
		
		filename = '{}.{}'.format(join(path[0], name), EXT)
		
		if not isfile(filename):
			return None
		
		return _MeatballLoader(fullname, filename)

class _ModuleLoader:
	def __init__(self, fullname, filename):
		self.fullname = fullname
		self.filename = filename
	
	def load_module(self, fullname):
		if self.fullname not in sys.modules:
			sys.modules[self.fullname] = self._load_module_from_file()
		return sys.modules[self.fullname]

class _MeatballLoader(_ModuleLoader):
	def _load_module_from_file(self):
		return compile_file(self.filename, self.fullname)

_finder = _MeatballFinder()

sys.meta_path.append(_finder)
