from .base import MeatballTestBase

from meatball.compiler import compile_template

class TestIntegration(MeatballTestBase):
	def test_statements(self):
		self.assertTemplateRenders('''
hello world
- (x, y) = (1, 2)
- if x == 1:
	foo bar
	- if y == 2:
		- for x in [1, 2]:
			- if x % 2 == 0:
				baz
			- else:
				bza
			bam
	bif
boom
''', '''hello world
foo bar
bza
bam
baz
bam
bif
boom''')
	
	def test_indents(self):
		self.assertTemplateRenders('''
<ul>
	- if True:
		<li>
			- if True:
				moo
			- end
		</li>
	- end
</ul>
''', '''<ul>
	<li>
		moo
	</li>
</ul>''')
	
	def test_header_import(self):
		# TODO: Not really testing that the imports are in the right place
		self.assertTemplateRenders('''
- import collections
- from collections import OrderedDict
hello
- from math import floor
{ floor(1.1) } { OrderedDict.__name__ } { collections.__name__ }
''', 'hello\n1 OrderedDict collections')
	
	def test_empty(self):
		self.assertTemplateRenders('', '')
	
	def test_empty_comment(self):
		self.assertTemplateRenders('- # nothing', '')
	
	def test_exprs(self):
		self.assertTemplateRenders('''
- x = 'a'
x{x}y
''', 'xay')
	
	def test_def_before_main(self):
		self.assertTemplateRenders('''
- def foo(self, x, y):
	X{x}Y{y}

Main: { self.foo('X', 'Y') }
''', 'Main: XXYY\n')
	
	def test_def_after_main(self):
		self.assertTemplateRenders('''
Main: { self.foo('X', 'Y') }

- def foo(self, x, y):
	X{x}Y{y}
''', 'Main: XXYY\n')
	
	def test_def_around_main(self):
		self.assertTemplateRenders('''
- def foo(self, x):
	X{x}

Main: { self.foo('X') } { self.bar('Y') }

- def bar(self, y):
	Y{y}
''', 'Main: XX\n YY\n')
	
	def test_def_indent(self):
		self.assertTemplateRenders('''
{ self.foo() }
	b
		c
			d
- def foo(self):
	a
''', 'a\n\tb\n\t\tc\n\t\t\td\n')
	
	def test_module(self):
		mod = compile_template('''- module
- def foo(a):
	({bar(a)})

- def bar(a):
	[{a}]
''')
		
		self.assertEqual(mod.foo('a'), '([a]\n)\n')
	
	def test_call(self):
		self.assertTemplateRenders('''
- call self.foo(1, arg = block)
	text

- def foo(self, a, arg):
	{a}{arg}
''', '1text\n')
		
		self.assertTemplateRenders('''
- call self.foo(1, b1, arg = b2)
	- arg b1
		text1
	
	- arg b2
		text2

- def foo(self, a, b, arg):
	{a}{b}{arg}
''', '1text1\ntext2\n')
	
	def test_call_nested(self):
		self.assertTemplateRenders('''
- call self.foo(block)
	a
	- call self.foo(block)
		b

- def foo(self, arg):
	{arg}
''', 'a\nb\n')
