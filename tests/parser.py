import unittest

from io import StringIO

from meatball.parser import Parser, ParseContext, ParseResult, ErrorHandler
from meatball import ast

class TestParser(unittest.TestCase):
	def test_template(self):
		res = self._parse_fine('''- extends foo.bar
- args a, b = 2

Hello, {a}! {self.f(a + b)}

- def f(self, x):
	\-{x}-
''')
		self.assertIsNotNone(res.class_)
		self.assertIsInstance(res.class_, ast.Class)
	
	def test_empty_module(self):
		res = self._parse_fine('- module')
		self.assertIsNone(res.class_)
	
	def test_module(self):
		res = self._parse_fine('''- module
- def foo(x, y):
	{x} {y}
	Coral
	{bar(x + y)}

- def bar(z, q = 2):
	{z} {q}
	Fields
''')
		self.assertIsNone(res.class_)
		self.assertEqual(
			len([x for x in res.module if isinstance(x, ast.Def)]),
			2
		)
	
	def test_call_single(self):
		self._parse_fine('''
- call foo.bar[0].bat(a, b, block, d, e = block, *[]):
	text
''')
	
	def test_call_multi(self):
		self._parse_fine('''
- call foo.bar[0].bat(a, b, c, d, e = e, *[]):
	- arg c
		c
	- arg e
		e
''')
	
	def _parse_fine(self, txt):
		eh = ErrorHandler()
		ctxt = ParseContext(error_handler = eh)
		p = Parser(ctxt, StringIO(txt))
		res = p.parse()
		if eh.errors:
			print(eh.errors)
		self.assertEqual(len(eh.errors), 0)
		self.assertIsNotNone(res)
		self.assertIsInstance(res, ParseResult)
		
		return res
