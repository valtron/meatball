from .integration import TestIntegration
from .parser import TestParser
from .magic import TestMagic
