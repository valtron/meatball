from io import StringIO

class Template:
	@classmethod
	def render(cls, *args, **kwargs):
		tmpl = cls()
		return tmpl(*args, **kwargs)
	
	def __call__(self, **data):
		return ''

class Outputter:
	def write(self, data):
		self.raw(data)
	
	def raw(self, text):
		raise NotImplementedError('Outputter.raw')
	
	def getvalue(self):
		raise NotImplementedError('Outputter.getvalue')

class StringOutputter(Outputter):
	def __init__(self):
		self._impl = StringIO()
	
	def raw(self, text):
		self._impl.write(str(text))
	
	def getvalue(self):
		return self._impl.getvalue()
