class Node:
	__slots__ = ()

class Named(Node):
	__slots__ = ('name',)
	
	def __init__(self, name):
		self.name = name

class Class(Named):
	__slots__ = ('bases', 'members',)
	
	def __init__(self, name, bases, members):
		super(Class, self).__init__(name)
		self.bases = bases
		self.members = members

class Def(Named):
	__slots__ = ('args', 'body',)
	
	def __init__(self, name, args, body):
		super(Def, self).__init__(name)
		self.args = args
		self.body = body

class Leaf(Node):
	__slots__ = ('text',)
	
	def __init__(self, text):
		super(Leaf, self).__init__()
		self.text = text

class Stmt(Node):
	pass

class Block(Stmt):
	__slots__ = ('head', 'body',)
	
	def __init__(self, head, body):
		super(Block, self).__init__()
		self.head = head
		self.body = body

class Code(Stmt):
	__slots__ = ('text',)
	
	def __init__(self, text):
		super(Code, self).__init__()
		self.text = text

class Call(Stmt):
	__slots__ = ('head', 'args',)
	
	def __init__(self, head, args):
		super(Call, self).__init__()
		self.head = head
		self.args = args

class CallArg(Node):
	__slots__ = ('name', 'value',)
	
	def __init__(self, name, value):
		super(CallArg, self).__init__()
		self.name = name
		self.value = value

class Expr(Leaf):
	pass

class Text(Leaf):
	pass

class Import(Node):
	__slots__ = ('imported', 'alias',)
	
	def __init__(self, imported, alias = None):
		self.imported = imported
		self.alias = alias

class Walker:
	def walk(self, node):
		node_type = type(node).__name__.lower()
		f = getattr(self, 'walk_{}'.format(node_type))
		f(node)
	
	def walk_class(self, node):
		self.walk_node(node)
	
	def walk_def(self, node):
		self.walk_node(node)
	
	def walk_import(self, node):
		self.walk_node(node)
	
	def walk_block(self, node):
		self.walk_stmt(node)
	
	def walk_code(self, node):
		self.walk_stmt(node)
	
	def walk_stmt(self, node):
		self.walk_node(node)
	
	def walk_expr(self, node):
		self.walk_node(node)
	
	def walk_text(self, node):
		self.walk_node(node)

class SourceBuilder(Walker):
	def __init__(self, outfh, indent = 0):
		self.outfh = outfh
		self.indent = indent
		self._outidx = 0
	
	def walk_class(self, node):
		self._line('class {}({}):'.format(node.name, ', '.join(node.bases)))
		self.indent += 1
		if node.members:
			for m in node.members:
				self.walk(m)
		else:
			self._line('pass')
		self.indent -= 1
	
	def walk_def(self, node):
		self._line('def {}({}):'.format(node.name, ', '.join(node.args)))
		self.indent += 1
		
		self._line('{} = _Outputter()'.format(self._outvar))
		for n in node.body:
			self.walk(n)
		self._line('return {}.getvalue()'.format(self._outvar))
		
		self.indent -= 1
	
	def walk_import(self, node):
		if node.alias:
			# TODO: This probably doesn't work with things involving
			# multiple leading dots ('...foo.bar.baz')
			
			imported = node.imported
			alias = node.alias
			
			(imported_mod, imported_name) = imported.rsplit('.', 1)
			
			if not imported_mod and imported.startswith('.'):
				imported_mod = '.'
			
			if imported_mod:
				import_line = 'from {} import {} as {}'.format(imported_mod, imported_name, alias)
			else:
				import_line = 'import {} as {}'.format(imported_name, alias)
			
			self._line(import_line)
		else:
			self._line('import {}'.format(node.imported))
	
	def walk_expr(self, node):
		self._write(node.text)
	
	def walk_text(self, node):
		self._line('{}.raw({})'.format(self._outvar, _pystr(node.text)))
	
	def walk_code(self, node):
		self._line(node.text)
	
	def walk_block(self, node):
		self._line(node.head)
		if node.body:
			self.indent += 1
			for n in node.body:
				self.walk(n)
			self.indent -= 1
	
	def walk_call(self, node):
		for arg in node.args:
			self.walk(arg)
		
		self._write(node.head)
	
	def walk_callarg(self, node):
		self._push_out()
		self._line('{} = _Outputter()'.format(self._outvar))
		for n in node.value:
			self.walk(n)
		self._line('{} = {}.getvalue()'.format(node.name, self._outvar))
		self._pop_out()
	
	def _write(self, expr):
		self._line('{}.write({})'.format(self._outvar, expr))
	
	def _line(self, txt):
		self.outfh.write('\t' * self.indent)
		self.outfh.write(txt)
		self.outfh.write('\n')
	
	@property
	def _outvar(self):
		return '_out{}'.format(self._outidx)
	
	def _push_out(self):
		self._outidx += 1
	
	def _pop_out(self):
		self._outidx -= 1

def _pystr(txt):
	return repr(txt)
