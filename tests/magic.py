from .base import MeatballTestBase

class TestMagic(MeatballTestBase):
	def test_magic_import(self):
		import meatball.magic
		
		from tests.testpkg import test_tmpl
		
		output = test_tmpl.render()
		self.assertEqual(output, 'Hello, world!\n')
	
	def test_extends(self):
		self.assertTemplateRenders('''
- extends tests.testpkg.test_base

x{super(_Template, self).__call__('a')}x
''', 'xaaax')
	
	def test_super_in_file(self):
		import meatball.magic
		
		from tests.testpkg import super_in_file
		
		output = super_in_file.render()
		self.assertEqual(output, 'xaaax')
	
	def test_relative_import(self):
		import meatball.magic
		
		from tests.testpkg import relative_import
		
		output = relative_import.render()
		self.assertEqual(output, 'xaaax')
