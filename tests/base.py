import unittest

from meatball.compiler import compile_template

class MeatballTestBase(unittest.TestCase):
	def assertTemplateRenders(self, tmpl_source, expected_output):
		tmpl = compile_template(tmpl_source.strip())
		output = tmpl.render()
		self.assertEqual(output, expected_output)
