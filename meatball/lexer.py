import re
from enum import Enum
from ply.lex import LexToken

class PlyLexerAdapter:
	def __init__(self, lexer):
		self._iter = iter(lexer)
	
	def token(self):
		try:
			return next(self._iter)
		except StopIteration:
			return None

class Lexer:
	def __init__(self, fh):
		self.fh = fh
	
	def __iter__(self):
		indent = Indent()
		
		stack = []
		
		for ind, t, txt in self._lines():
			if txt in ('', '\n'):
				continue
			
			ind_diff = ind - indent.total
			code_ind = 0
			
			if ind_diff > 0:
				il = stack[-1]
				il.width += ind_diff
				indent.add(il.type, ind_diff)
				
				if il.type == LineType.CODE:
					code_ind = ind_diff
			elif ind_diff < 0:
				while indent.total > ind:
					il = stack.pop()
					indent.add(il.type, -il.width)
					if il.type == LineType.CODE:
						code_ind -= il.width
			
			if code_ind:
				tokt = TokenType.DEDENT if code_ind < 0 else TokenType.INDENT
				
				for _ in range(abs(code_ind)):
					yield self._token(tokt)
			
			if t == LineType.CODE:
				if txt == 'end':
					continue
				
				if txt.startswith('def '):
					tokt = TokenType.DEF
					txt = txt[3:].strip()
				elif txt.startswith('args '):
					tokt = TokenType.ARGS
					txt = txt[4:].strip()
				elif txt.startswith('extends '):
					tokt = TokenType.EXTENDS
					txt = txt[7:].strip()
				elif txt.startswith('call '):
					tokt = TokenType.CALL
					txt = txt[4:].strip()
				elif txt.startswith('arg '):
					tokt = TokenType.ARGNAME
					txt = txt[3:].strip()
				elif txt == 'module':
					tokt = TokenType.MODULE
					txt = None
				else:
					tokt = TokenType.CODE
				
				yield self._token(tokt, txt)
			elif t == LineType.TEXT:
				txt = '\t' * indent.text + txt
				
				for tokt, data in self._split_exprs(txt):
					yield self._token(tokt, data)
			
			if stack and not stack[-1].width:
				stack.pop()
			
			stack.append(IndentLevel(t))
		
		for _ in range(indent.code):
			yield self._token(TokenType.DEDENT)
	
	def _token(self, type, data = None):
		t = LexToken()
		t.type = type.value
		t.value = data
		t.lexer = self
		t.lineno = 0
		t.lexpos = 0
		return t
	
	def _lines(self):
		for line in self.fh:
			txt = line.lstrip('\t')
			ind = len(line) - len(txt)
			
			if txt.startswith('- '):
				txt = txt[2:].strip()
				t = LineType.CODE
			else:
				if txt.startswith('\\'):
					txt = txt[1:]
				t = LineType.TEXT
			
			yield ind, t, txt
	
	def _split_exprs(self, txt):
		for m in re.finditer(r'([^{]+)|\{([^}]*)\}', txt):
			tt = m.group(1)
			te = m.group(2)
			
			if tt:
				yield TokenType.TEXT, tt
			else:
				yield TokenType.EXPR, te.strip()

class Indent:
	__slots__ = ('code', 'text', 'total')
	
	def __init__(self, code = None, text = None):
		self.code = code or 0
		self.text = text or 0
		self.total = self.code + self.text
	
	def add(self, type, width):
		if type == LineType.TEXT:
			self.text += width
		else:
			self.code += width
		self.total += width

class IndentLevel:
	__slots__ = ('type', 'width')
	
	def __init__(self, type, width = None):
		self.type = type
		self.width = width or 0

class LineType(Enum):
	TEXT = 1
	CODE = 2

class TokenType(Enum):
	TEXT = 'TEXT'
	CODE = 'CODE'
	EXPR = 'EXPR'
	DEF = 'DEF'
	INDENT = 'INDENT'
	DEDENT = 'DEDENT'
	ARGS = 'ARGS'
	EXTENDS = 'EXTENDS'
	MODULE = 'MODULE'
	CALL = 'CALL'
	ARGNAME = 'ARGNAME'
