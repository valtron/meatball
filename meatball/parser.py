import re
from sys import stderr
from ply.yacc import yacc

from . import ast, lexer, config

class ParseContext:
	def __init__(self, template_name = None, error_handler = None):
		self.error_handler = error_handler or PrintErrorHandler()
		self.template_name = template_name or '_Template'

class ParseResult:
	def __init__(self, class_=  None, module = None):
		self.class_ = class_
		self.module = module

class Parser:
	tokens = tuple(tt.value for tt in lexer.TokenType)
	
	def __init__(self, parse_context, stream):
		self._context = parse_context
		
		self._lexer = lexer.PlyLexerAdapter(lexer.Lexer(stream))
		self._impl = yacc(module = self, start = 'p_file', debug = False)
	
	def parse(self):
		return self._impl.parse(None, lexer = self._lexer)
	
	def p_file(self, p):
		'''
			p_file : p_template
				| p_module
		'''
		p[0] = p[1]
	
	def p_template(self, p):
		'''
			p_template : p_extends_opt p_args_opt p_template_part_list
		'''
		
		# TODO: This logic shouldn't be in the parser
		
		base = p[1] or 'meatball.utils.Template'
		base_alias = '_TemplateBase'
		name = self._context.template_name
		main_method_args = (p[2] or 'self,**data').split(',')
		
		main_method_body = []
		
		members = []
		
		for part in p[3]:
			if isinstance(part, ast.Def):
				members.append(part)
			else:
				main_method_body.append(part)
		
		main_method = ast.Def(
			'__call__', main_method_args, main_method_body,
		)
		
		members.append(main_method)
		
		class_ = ast.Class(name, [base_alias], members)
		marker = ast.Code('{} = {}'.format(config.MARKER, name))
		
		module = [
			ast.Import('meatball.magic'),
			ast.Import('meatball.utils.StringOutputter', '_Outputter'),
			ast.Import(base, base_alias),
		]
		
		module.extend([
			class_, marker
		])
		
		p[0] = ParseResult(class_, module)
	
	def p_extends_opt(self, p):
		'''
			p_extends_opt : EXTENDS
				|
		'''
		
		if len(p) > 1:
			p[0] = p[1]
	
	def p_args_opt(self, p):
		'''
			p_args_opt : ARGS
				|
		'''
		
		if len(p) > 1:
			p[0] = p[1]
	
	def p_template_part_list(self, p):
		'''
			p_template_part_list : p_template_part p_template_part_list
				|
		'''
		
		if len(p) == 1:
			p[0] = []
		else:
			p[0] = [p[1]] + p[2]
	
	def p_template_part(self, p):
		'''
			p_template_part : p_body_part
				| p_func
		'''
		p[0] = p[1]
	
	def p_module(self, p):
		'''
			p_module : MODULE p_module_part_list
		'''
		
		# TODO: This logic shouldn't be in the parser
		
		module = [
			ast.Import('meatball.magic'),
			ast.Import('meatball.utils.StringOutputter', '_Outputter'),
		]
		module.extend(p[2])
		
		p[0] = ParseResult(None, module)
	
	def p_module_part_list(self, p):
		'''
			p_module_part_list : p_module_part p_module_part_list
				|
		'''
		
		if len(p) == 1:
			p[0] = []
		else:
			p[0] = [p[1]] + p[2]
	
	def p_module_part(self, p):
		'''
			p_module_part : p_func
		'''
		p[0] = p[1]
	
	def p_func(self, p):
		'''
			p_func : DEF p_block
		'''
		
		m = re.match(r'(\w+)\s*\((.*)\)\s*:', p[1])
		
		p[0] = ast.Def(
			m.group(1), map(str.strip, m.group(2).split(',')), p[2]
		)
	
	def p_body(self, p):
		'''
			p_body : p_body_part p_body
				| p_body_part
		'''
		
		p[0] = [p[1]]
		
		if len(p) > 2:
			p[0] += p[2]
	
	def p_body_part_text(self, p):
		'''
			p_body_part : TEXT
		'''
		p[0] = ast.Text(p[1])
	
	def p_body_part_expr(self, p):
		'''
			p_body_part : EXPR
		'''
		p[0] = ast.Expr(p[1])
	
	def p_body_part_code(self, p):
		'''
			p_body_part : CODE p_block_opt
		'''
		p[0] = ast.Block(p[1], p[2])
	
	def p_body_part_call(self, p):
		'''
			p_body_part : CALL p_call_args
		'''
		p[0] = ast.Call(p[1], p[2])
	
	def p_call_args(self, p):
		'''
			p_call_args : p_call_args_single
				| INDENT p_call_args_multi DEDENT
		'''
		
		if len(p) == 2:
			p[0] = p[1]
		else:
			p[0] = p[2]
	
	def p_call_args_single(self, p):
		'''
			p_call_args_single : p_block
		'''
		p[0] = [ast.CallArg('block', p[1])]
	
	def p_call_args_multi(self, p):
		'''
			p_call_args_multi :
				| p_call_arg p_call_args_multi
		'''
		
		if len(p) > 1:
			p[0] = [p[1]] + p[2]
		else:
			p[0] = []
	
	def p_call_arg(self, p):
		'''
			p_call_arg : ARGNAME p_block
		'''
		p[0] = ast.CallArg(p[1], p[2])
	
	def p_block_opt(self, p):
		'''
			p_block_opt : p_block
				|
		'''
		
		if len(p) > 1:
			p[0] = p[1]
	
	def p_block(self, p):
		'''
			p_block : INDENT p_body DEDENT
		'''
		p[0] = p[2]
	
	def p_error(self, p):
		msg = "Error: {}".format(p)
		self._context.error_handler.error(msg)

class ErrorHandler:
	def __init__(self):
		self.errors = []
	
	def error(self, msg):
		self.errors.append(msg)

class PrintErrorHandler:
	def error(self, msg):
		stderr.write(msg)
		stderr.write('\n')
