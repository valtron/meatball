import re
import imp
import marshal
from time import ctime
from io import StringIO
from os.path import splitext, exists, getmtime

from .parser import Parser, ParseContext
from . import ast, config

def compile_template(fh):
	if isinstance(fh, str):
		fh = StringIO(fh)
	
	bytecode = _compile_meatball_to_bytecode(fh, '<string>')
	return _bytecode_to_module(bytecode)

def compile_file(filename, fullname):
	filebase = splitext(filename)[0]
	compiled_file = '{}.{}'.format(filebase, config.EXTC)
	if _should_recompile(filename, compiled_file):
		with open(filename, 'r') as fh:
			bytecode = _compile_meatball_to_bytecode(fh, filename)
		with open(compiled_file, 'wb') as fh:
			marshal.dump(bytecode, fh)
	else:
		with open(compiled_file, 'rb') as fh:
			bytecode = marshal.load(fh)
	return _bytecode_to_module(bytecode, fullname)

def _compile_meatball_to_bytecode(fh, filename):
	source = _build_template_source(fh)
	return compile(source, filename, 'exec')

def _should_recompile(filename, compiled_file):
	try:
		m2 = ctime(getmtime(compiled_file))
	except: # TODO: Don't catch everything
		return True
	m1 = ctime(getmtime(filename))
	return m1 >= m2

def _bytecode_to_module(bytecode, module_name = None):
	mod = imp.new_module(module_name or '<meatball>')
	exec(bytecode, mod.__dict__)
	template = mod.__dict__.get(config.MARKER)
	
	if template:
		# TODO: If `mod` gets GC'd, (which it will barring this reference)
		# global lookups (i.e. imports) done from within the methods
		# return `None`. Need to fix.
		template.__hacky_hack_hack = mod
		return template
	
	return mod

def _build_template_source(fh):
	s = StringIO()
	
	context = ParseContext()
	parser = Parser(context, fh)
	res = parser.parse()
	
	for node in res.module:
		sb = ast.SourceBuilder(s)
		sb.walk(node)
	
	return s.getvalue()
