from distutils.core import setup

setup(
	name = "meatball",
	version = '0.1.2',
	author = 'valtron',
	author_email = 'valtron2000+meatball@gmail.com',
	packages = ['meatball'],
	url = 'https://github.com/valtron/meatball',
	license = 'BSD',
	description = "Python template engine",
	zip_safe = True,
	requires = [
		'ply >= 3.9',
	],
	classifiers = [
		'Development Status :: 3 - Alpha',
		'Programming Language :: Python :: 3 :: Only',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: BSD License',
		'Operating System :: OS Independent',
		'Topic :: Text Processing :: General',
		'Topic :: Software Development :: Libraries',
	]
)
